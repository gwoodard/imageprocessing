%Name: George Woodard
%Class: Image Processing CS 563
%Date: 2/6/2015

A = [1 2 5; 3 2 6; 7 8 5];
B = sum(A);
C = sum(B);
D = sum(A(1:6));
H = sum(D);
v([3 4 5]) = [0 0 0];
F = fliplr(A);