# Image Processing Assignments #

### **About:** ###

Source code with screenshot output and documentation of assignments for an Image Processing class using MATLAB. In this class we used general concepts of image processing, sensing, sampling and quantization, image segmentation and edge detection, image sequence analysis, image enhancement and restoration, image understanding
systems, applications of mathematical morphology. 

### **IDE:**
MATLAB

### **Input:** ###
various MATLAB scripts

### **Language:** ###
MATLAB