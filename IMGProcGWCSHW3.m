%Name: George Woodard
%Class: Image Processing CS 561
 
% This code reads the image from the file then displays it
f = imread('C:\Users\student\Desktop\Image Processing\grayscaleLion.jpg');
imshow(f);
% This code takes the above image then convert it to a binary (Black and
% White) image and then displays it.
%b = im2bw(f, 0.6);
%imshow(b);